import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AttractionInterface } from '../Interface/attraction.interface';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  
  constructor(private http: HttpClient) { }

  public getData(url: string) {
    let data = this.http.get(url);
    return data;
  }

  public postData(url: string, data: any) {
    let result = this.http.post(url, data);
    return result;
  }

  public deleteData(url: string) {
    let result = this.http.delete(url);
    return result;
  }

  public putData(url: string, attraction: AttractionInterface){
    let result = this.http.put(url,attraction);
    return result;
  }
}